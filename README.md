# README

* Ruby version: 2.4.0p0

* Rails version: 5.0.3

* System dependencies:
 - gem devise
 - gem bulma-rails - v: 0.4.2
 - gem friendly_id - v: 5.1.0

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions
