module Blog
  class PagesController < BlogController

    def about
    end

    def contact
    end

    def gifs
      # recebe a string que foi digitada no input como parametro
      @search_gif = params[:giphy][:gif] if params[:giphy].present?
      # salva na variavel giphys o resultado do metodo de busca usando o parametro digitado
      @giphys = Giphy.search_items(@search_gif)
    end
  end
end