class Post < ApplicationRecord

  acts_as_taggable # Alias for acts_as_taggable_on :tags

  extend FriendlyId
  friendly_id :title, use: :slugged

  belongs_to :author

  PER_PAGE = 5

  # ordenara as postagens na ordem decrescente quando forem criadas
  scope :most_recent, -> { order(published_at: :desc) }

  scope :published, -> {where(published: true)}
  scope :recent_paginated, -> (page) { most_recent.page(page).per(PER_PAGE) }
  scope :with_tag, -> (tag) { tagged_with(tag) if tag.present? }
  scope :list_for, -> (page, tag) do
    recent_paginated(page).with_tag(tag)
  end

  # verifica se o titulo do post foi alterado e altera na url amigavel
  def should_generate_new_friendly_id?
    title_changed?
  end

  # mostra a data e hora de publicacao da postagem
  def display_day_published
    if published_at.present?
      "Publicado em #{published_at.strftime('%-d/%-m/%-Y as %-H:%-M')}"
    else
      "Postagem não publicada ainda."
    end
  end

  def publish
    update(published: true, published_at: Time.now)
  end

  def unpublish
    update(published: false, published_at: nil)
  end

end
