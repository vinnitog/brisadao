class Author < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, #:registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # um autor pode ter varias postagens
  has_many :posts

  # exige que seja passado o parametro nome para realizar o update do autor
  validates_presence_of :name, on: :update

  # printa o nome do autor ou 'autor' como default
  def display_name
    name.present? ? name : 'Autor'
  end
end
