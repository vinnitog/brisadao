class Giphy < ApplicationRecord
  include HTTParty

  # url base da api de gifs utilizada
  base_uri 'https://api.giphy.com/v1/gifs'
  # parametros necessarios para utilizacao da api
  default_params api_key: '250b38ae3f3040e3b626254166186990', limit: 50, offset: 0, rating: 'G', lang: 'en'
  format :json

  def self.search_items(gif)
    get('/search', query: { q: gif })["data"]
  end
end
