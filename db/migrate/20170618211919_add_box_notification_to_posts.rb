class AddBoxNotificationToPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :box_notification, :string
  end
end
