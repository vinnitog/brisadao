Rails.application.routes.draw do

  devise_for :authors
  root to: 'blog/posts#index'

  namespace :authors do
    get '/account' => 'accounts#edit', as: :account
    put '/info' => 'accounts#update_info', as: :info
    put '/change_password' => 'accounts#change_password', as: :change_password
    resources :posts do
      put 'publish' => 'posts#publish', on: :member
      put 'unpublish' => 'posts#unpublish', on: :member
    end
  end

  scope module: 'blog' do
    get 'about' => 'pages#about', as: :about, path: 'sobre'
    get 'contact' => 'pages#contact', as: :contact, path: 'contato'
    get 'gifs' => 'pages#gifs', as: :giphy, path: 'gifs'
    get 'posts' => 'posts#index', as: :posts, path: 'postagens'
    # get 'posts/:id' => 'posts#show', as: :post
    scope :path => '/postagens', :controller => :posts do
      get '/:id' => :show, :as => 'post'
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
